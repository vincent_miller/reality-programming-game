using System;
using System.Collections.Generic;
using UnityEngine;

public class CaretCakeParser {

	private CaretCakeTokenizer tkn;

	public CaretCakeParser (string data) {
		this.tkn = new CaretCakeTokenizer (data);
	}

	public bool accept(CaretCakeTokenizer.Token t) {
		CaretCakeTokenizer.Token found = tkn.nextToken();
		if (found == t) {
			return true;
		} else {
			tkn.push(found);
			return false;
		}
	}

	public Statements.Statement nextStatement() {
		Expressions.Expression ex = nextExpression ();
		if (ex == null) {
			Debug.LogError ("Failed 26");
			return null;
		}
		if (ex is Expressions.VariableFetchExpression && accept (CaretCakeTokenizer.Token.SET)) {
			Expressions.Expression expr = nextExpression();
			if (expr == null) {
				return null;
			}
			return new Statements.BindStatement(((Expressions.VariableFetchExpression) ex).var, expr);
		}
		return new Statements.ExpressionStatement (ex);
	}

	public Expressions.Expression nextExpression() {
		Expressions.Expression term = nextTerm();
		if (term == null) {
			Debug.LogError("Failed 35");
			return null;
		}
		while (true) {
			if (accept (CaretCakeTokenizer.Token.DOT)) {
				if (!accept (CaretCakeTokenizer.Token.SYMBOL)) {
					Debug.LogError("Failed 41");
					return null;
				}
				term = new Expressions.FieldFetchExpression(term, (string) tkn.getAssociated());
			} else if (accept (CaretCakeTokenizer.Token.LP)) {
				List<Expressions.Expression> arguments = new List<Expressions.Expression>();
				if (!accept(CaretCakeTokenizer.Token.RP)) {
					// Argument list
					do {
						Expressions.Expression e = nextExpression();
						if (e == null) {
							Debug.LogError("Failed 52");
							return null;
						}
						arguments.Add(e);
					} while (accept(CaretCakeTokenizer.Token.COMMA));
					if (!accept(CaretCakeTokenizer.Token.RP)) {
						Debug.LogError("Failed 58");
						return null;
					}
				}
				term = new Expressions.InvokeExpression(term, arguments);
			} else {
				return term;
			}
		}
	}

	public Expressions.Expression nextTerm() {
		if (accept (CaretCakeTokenizer.Token.NUMBER)) {
			return new Expressions.NumberConstantExpression ((float) tkn.getAssociated ());
		} else if (accept (CaretCakeTokenizer.Token.SYMBOL)) {
			return new Expressions.VariableFetchExpression ((string) tkn.getAssociated ());
		} else if (accept (CaretCakeTokenizer.Token.LP)) {
			Expressions.Expression outp = nextExpression ();
			if (outp == null || !accept (CaretCakeTokenizer.Token.RP)) {
				Debug.LogError("Failed 77");
				return null;
			}
			return outp;
		} else {
			CaretCakeTokenizer.Token peek = tkn.nextToken();
			tkn.push(peek);
			Debug.LogError("Failed 84 with " + peek);
			return null;
		}
	}
}
