
public class ZeroAccessibilityProvider : AccessibilityProvider {
	public bool CanMove () {
		return false;
	}

	public bool CanImpel () {
		return false;
	}

	public bool CanRotate () {
		return false;
	}

	public bool CanFace () {
		return false;
	}

	public bool CanScale () {
		return false;
	}

	public bool CanKill() {
		return false;
	}
}
