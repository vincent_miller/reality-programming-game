using System;
using UnityEngine;

public class VirtualObjects {
	public interface LivingObject {
		bool IsAlive();
	}

	public class SecondWaitVO : LivingObject {
		private DateTime begin = DateTime.Now;

		public bool IsAlive() {
			bool outp = (DateTime.Now - begin).TotalMilliseconds < 1000;
			//Debug.Log ("Living " + outp + " -> " + (DateTime.Now - begin));
			return outp;
		}
	}

	public class ImporterVO {
	}
}
