﻿using UnityEngine;
using System.Collections;

public class Transducer : MonoBehaviour {
	
	public GameObject Notify1;
	public string OnNotify1;
	public GameObject Notify2;
	public string OnNotify2;
	public GameObject Notify3;
	public string OnNotify3;
	public GameObject Notify4;
	public string OnNotify4;
	
	public void actuate(int id) {
		switch (id & 3) {
		case 0: if (Notify4 != null) { Notify4.GetComponent<Triggerable>().OnTrigger(OnNotify4); } break;
		case 1: if (Notify1 != null) { Notify1.GetComponent<Triggerable>().OnTrigger(OnNotify1); } break;
		case 2: if (Notify2 != null) { Notify2.GetComponent<Triggerable>().OnTrigger(OnNotify2); } break;
		case 3: if (Notify3 != null) { Notify3.GetComponent<Triggerable>().OnTrigger(OnNotify3); } break;
		}
	}
}
