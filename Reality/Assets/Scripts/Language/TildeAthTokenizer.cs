using System;
using UnityEngine;

public class TildeAthTokenizer {

	public enum Token {
		SYMBOL, NUMBER, LP, RP, LCB, RCB, SEMI, DOT, COMMA, ERROR, END_OF_FILE,
		IMPORT, TILDEATH, EXECUTE, NULL, THIS, BIND, SET
	}

	private string data;
	private int index = 0;
	private object attached;
	private Token pushback = Token.END_OF_FILE;
	public const string VALID_IDENTIFIER = "~^ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";

	public TildeAthTokenizer (string data) {
		this.data = data;
	}

	public void push(Token t) {
		if (pushback != Token.END_OF_FILE) {
			throw new Exception ("Already got one pushback!");
		}
		pushback = t;
	}

	public Token nextToken() {
		if (pushback != Token.END_OF_FILE) {
			Token outp = pushback;
			pushback = Token.END_OF_FILE;
			return outp;
		}
	begin:
		if (index >= data.Length) {
			return Token.END_OF_FILE;
		}
		attached = null;
		char c = data [index++];
		switch (c) {
		case '(': return Token.LP;
		case ')': return Token.RP;
		case '{': return Token.LCB;
		case '}': return Token.RCB;
		case '.': return Token.DOT;
		case ',': return Token.COMMA;
		case ';': return Token.SEMI;
		case '=': return Token.SET;
		case ' ':
		case '\n':
		case '\r':
		case '\t': goto begin;
		}
		if (c >= '0' && c <= '9' || c == '-') {
			bool negate = c == '-';
			bool isfloat = false;
			int shift = 0;
			float value = negate ? 0 : c - '0';
			while (true) {
				c = data [index];
				if (c == '.') {
					if (isfloat) {
						attached = "Bad number!";
						index = data.Length;
						return Token.ERROR;
					}
					isfloat = true;
				} else if (c < '0' || c > '9') {
					break;
				}
				value = value * 10 + c - '0';
				index++;
				if (isfloat) { shift++; }
			}
			while (shift-- > 0) { value /= 10; }
			attached = negate ? -value : value;
			return Token.NUMBER;
		} else {
			int begin = index - 1;
			index = begin;
			while (index < data.Length && VALID_IDENTIFIER.Contains(char.ToString(data[index]))) {
				index++;
			}
			if (index - begin <= 0) {
				attached = "Bad identifier!";
				index = data.Length;
				return Token.ERROR;
			}
			attached = data.Substring(begin, index - begin);
			switch ((string) attached) {
			case "import": return Token.IMPORT; 
			case "~ATH": return Token.TILDEATH;
			case "EXECUTE": return Token.EXECUTE;
			case "NULL": return Token.NULL;
			case "THIS": return Token.THIS;
			case "BIND": return Token.BIND;
			default: return Token.SYMBOL;
			}
		}
	}

	public object getAssociated() {
		return attached;
	}
}
