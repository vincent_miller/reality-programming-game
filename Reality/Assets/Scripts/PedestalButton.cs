using UnityEngine;
using System;

public class PedestalButton : MonoBehaviour {

	public GameObject Notify;
	public string OnPress;
	public string OnRelease;
	public float timeSeconds = 0;
	public DateTime start = DateTime.MinValue;
	public AudioClip pressedSound;

	private bool pressed;

	void Update() {
		if (timeSeconds != 0 && pressed && (DateTime.Now - start).TotalSeconds >= timeSeconds) {
			if (Notify != null) {
				Notify.GetComponent<Triggerable>().OnTrigger(OnRelease);
			}
			pressed = false;
		}
		if (Input.GetKeyDown(KeyCode.E)) {
			RaycastHit info;
			if (Physics.Raycast(Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2)), out info, 2)) {
				if (info.transform.gameObject == this.gameObject) {
					AudioSource.PlayClipAtPoint(pressedSound, transform.position);
					if (pressed) {
						if (timeSeconds == 0) {
							if (Notify != null) {
								Notify.GetComponent<Triggerable>().OnTrigger(OnRelease);
							}
							pressed = false;
						}
					} else {
						if (Notify != null) {
							Notify.GetComponent<Triggerable>().OnTrigger(OnPress);
						}
						pressed = true;
						if (timeSeconds != 0) {
							start = DateTime.Now;
						}
					}
				}
			}
		}
	}
}