﻿using UnityEngine;
using System.Collections;

public class Door : Triggerable {
	public float speed = 4f;
	public AudioClip openSound;
	public AudioClip closeSound;

	GameObject leftDoor;
	GameObject rightDoor;

	public bool open = false;

	private Vector3 leftOpen;

	private Vector3 leftClose;

	private Vector3 rightOpen;

	private Vector3 rightClose;


	// Use this for initialization
	void Start () {
		leftDoor = transform.FindChild("doorleft").gameObject;
		rightDoor = transform.FindChild("doorright").gameObject;

		leftOpen = leftDoor.transform.position + leftDoor.transform.right * leftDoor.GetComponent<BoxCollider>().size.x * transform.localScale.x;
		leftClose = leftDoor.transform.position;
		rightOpen = rightDoor.transform.position - rightDoor.transform.right * rightDoor.GetComponent<BoxCollider>().size.x * transform.localScale.x;
		rightClose = rightDoor.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (open) {
			if (leftDoor.transform.position != leftOpen) {
				leftDoor.transform.position = Vector3.MoveTowards(leftDoor.transform.position, leftOpen, Time.deltaTime * speed);
			}

			if (rightDoor.transform.position != rightOpen) {
				rightDoor.transform.position = Vector3.MoveTowards(rightDoor.transform.position, rightOpen, Time.deltaTime * speed);
			}
		} else {
			if (leftDoor.transform.position != leftClose) {
				leftDoor.transform.position = Vector3.MoveTowards(leftDoor.transform.position, leftClose, Time.deltaTime * speed);
			}
			
			if (rightDoor.transform.position != rightClose) {
				rightDoor.transform.position = Vector3.MoveTowards(rightDoor.transform.position, rightClose, Time.deltaTime * speed);
			}
		}
	}


	public override void OnTrigger(string command) {
		if (command == "open" && !open) {
			open = true;
			AudioSource.PlayClipAtPoint(openSound, transform.position);
		}

		if (command == "close") {
			open = false;
			AudioSource.PlayClipAtPoint(closeSound, transform.position);
		}
	}
}
