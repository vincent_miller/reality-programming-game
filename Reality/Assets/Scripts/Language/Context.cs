using System;
using System.Collections.Generic;
using UnityEngine;

public class Context {
	public static AccessibilityProvider GetAccess(ComputerExecution ex, GameObject o) {
		AccessibilityProvider p = o.GetComponent<AllowModification>();
		if (p == null) {
			p = new ZeroAccessibilityProvider ();
		}
		return p;
	}

	public static void SetupTildeAthVars(Dictionary<string, object> vars) {
		vars ["print"] = new VirtualMethod (null, VirtualMethodType.PRINT);
	}
	
	public static void SetupCaretCakeVars(Dictionary<string, object> vars) {
		vars ["print"] = new VirtualMethod (null, VirtualMethodType.PRINT);
		vars ["world"] = new VirtualObjects.ImporterVO();
	}

	public static object Import(ComputerExecution ex, string typename, string varname) {
		if (typename == "author") {
			return GameObject.FindGameObjectWithTag ("Player");
		} else if (typename == "second") {
			return new VirtualObjects.SecondWaitVO();
		}
		GameObject best = null;
		foreach (GameObject found in GameObject.FindObjectsOfType<GameObject>()) {
			if (found.name.ToLower() == typename) {
				if (best == null ||
				    Vector3.Distance(ex.transform.position, found.transform.position)
				    < Vector3.Distance(ex.transform.position, best.transform.position)) {
					best = found;
				}
			}
		}
		if (best == null) {
			Debug.LogError("Not found: " + typename + " / " + varname);
		}
		return best;
	}

	public static object FieldFetchTildeAth(ComputerExecution ex, object obj, string var) {
		if (obj is GameObject) {
			GameObject go = (GameObject) obj;
			switch (var) {
			case "MOVE": if (GetAccess(ex, go).CanMove()) { return new VirtualMethod(obj, VirtualMethodType.MOVE); } break;
			case "IMPEL": if (GetAccess(ex, go).CanImpel()) { return new VirtualMethod(obj, VirtualMethodType.IMPEL); } break;
			case "ROTATE": if (GetAccess(ex, go).CanRotate()) { return new VirtualMethod(obj, VirtualMethodType.ROTATE); } break;
			case "FACE": if (GetAccess(ex, go).CanFace()) { return new VirtualMethod(obj, VirtualMethodType.FACE); } break;
			case "SCALE": if (GetAccess(ex, go).CanScale()) { return new VirtualMethod(obj, VirtualMethodType.SCALE); } break;
			case "ACTUATE": return new VirtualMethod(obj, VirtualMethodType.ACTUATE);
			case "DIE":  if (GetAccess(ex, go).CanKill()) { return new VirtualMethod(obj, VirtualMethodType.KILL); } break;
			case "transform": return go.transform;
			}
		} else if (obj is Transform) {
			Transform tf = (Transform) obj;
			switch (var) {
			case "forward": return tf.forward;
			case "object": return tf.gameObject;
			case "position": return tf.position;
			case "up": return tf.up;
			case "right": return tf.right;
			}
		} else if (obj is ComputerExecution) {
			switch (var) {
			case "DIE": return new VirtualMethod(obj, VirtualMethodType.DIE);
			}
		}
		Debug.LogError("Not found: " + obj + "." + var);
		return null;
	}

	public static object FieldFetchCaretCake(ComputerExecution ex, object obj, string var) {
		if (obj is GameObject) {
			GameObject go = (GameObject) obj;
			switch (var) {
			case "move": if (GetAccess(ex, go).CanMove()) { return new VirtualMethod(obj, VirtualMethodType.MOVE); } break;
			case "impel": if (GetAccess(ex, go).CanImpel()) { return new VirtualMethod(obj, VirtualMethodType.IMPEL); } break;
			case "rotate": if (GetAccess(ex, go).CanRotate()) { return new VirtualMethod(obj, VirtualMethodType.ROTATE); } break;
			case "face": if (GetAccess(ex, go).CanFace()) { return new VirtualMethod(obj, VirtualMethodType.FACE); } break;
			case "scale": if (GetAccess(ex, go).CanScale()) { return new VirtualMethod(obj, VirtualMethodType.SCALE); } break;
			case "kill": if (GetAccess(ex, go).CanKill()) { return new VirtualMethod(obj, VirtualMethodType.KILL); } break;
			case "actuate": return new VirtualMethod(obj, VirtualMethodType.ACTUATE);
			}
		} else if (obj is VirtualObjects.ImporterVO) {
			return Import(ex, var, var);
		}
		Debug.LogError("Not found: " + obj + "." + var);
		return null;
	}

	public static object Invoke(object obj, object[] args) {
		if (obj is VirtualMethod) {
			return ((VirtualMethod) obj).Invoke(args);
		}
		Debug.LogError("Not invokable: " + obj);
		return null;
	}

	public static object GetThis(ComputerExecution ex) {
		return ex;
	}

	public static bool IsAlive(object o) {
		if (o is VirtualObjects.LivingObject) {
			return ((VirtualObjects.LivingObject)o).IsAlive ();
		} else if (o is GameObject) {
			return ((GameObject) o).gameObject != null;
		}
		return o != null;
	}

	public static int GetDeathEnergy(object o) {
		return 0;
	}

	public enum VirtualMethodType {
		MOVE, DIE, ROTATE, IMPEL, PRINT, FACE, SCALE, ACTUATE, KILL
	}

	public class VirtualMethod {
		private object obj;
		private VirtualMethodType type;
		public VirtualMethod(object obj, VirtualMethodType type) {
			this.obj = obj;
			this.type = type;
		}

		public object Invoke(object[] args) {
			switch (type) {
			case VirtualMethodType.MOVE:
				if (args.Length != 3 || !(args[0] is float) || !(args[1] is float) || !(args[2] is float)) {
					Debug.LogError("Bad arguments to MOVE()!");
				} else {
					((GameObject) obj).transform.Translate(new Vector3((float) args[0], (float) args[1], (float) args[2]));
				}
				return null;
			case VirtualMethodType.ROTATE:
				if (args.Length != 3 || !(args[0] is float) || !(args[1] is float) || !(args[2] is float)) {
					Debug.LogError("Bad arguments to ROTATE()!");
				} else {
					((GameObject) obj).transform.Rotate(new Vector3((float) args[0], (float) args[1], (float) args[2]));
				}
				return null;
			case VirtualMethodType.IMPEL:
				if (args.Length != 3 || !(args[0] is float) || !(args[1] is float) || !(args[2] is float)) {
					Debug.LogError("Bad arguments to IMPEL()!");
				} else {
					Rigidbody b = ((GameObject) obj).rigidbody;
					if (b != null) {
						b.velocity += new Vector3((float) args[0], (float) args[1], (float) args[2]);
					}
				}
				return null;
			case VirtualMethodType.FACE:
				if (args.Length != 3 || !(args[0] is float) || !(args[1] is float) || !(args[2] is float)) {
					Debug.LogError("Bad arguments to FACE()!");
				} else {
					((GameObject) obj).transform.LookAt(new Vector3((float) args[0], (float) args[1], (float) args[2]));
				}
				return null;
			case VirtualMethodType.SCALE:
				if (args.Length != 3 || !(args[0] is float) || !(args[1] is float) || !(args[2] is float)) {
					Debug.LogError("Bad arguments to SCALE()!");
				} else {
					((GameObject) obj).transform.localScale = new Vector3((float) args[0], (float) args[1], (float) args[2]);
				}
				return null;
			case VirtualMethodType.DIE:
				if (args.Length != 0) {
					Debug.LogError("Bad arguments to DIE()!");
				} else {
					((ComputerExecution) obj).stop ();
				}
				return null;
			case VirtualMethodType.PRINT:
				string outp = "Program:";
				foreach (object arg in args) {
					outp += " " + arg;
				}
				Debug.Log(outp);
				return null;
			case VirtualMethodType.ACTUATE:
				if (args.Length > 1) {
					Debug.LogError("Bad arguments to ACTUATE()!");
				} else {
					Transducer trns = ((GameObject) obj).GetComponent<Transducer>();
					if (trns != null) {
						trns.actuate(args.Length > 0 ? (int) (float) args[0] : 1);
					}
				}
				return null;
			case VirtualMethodType.KILL:
				if (args.Length != 0) {
					Debug.LogError("Bad arguments to KILL()!");
				} else {
					MonoBehaviour.Destroy((GameObject) obj);
				}
				return null;
			default:
				Debug.LogError ("Invalid VMT: " + type);
				return null;
			}
		}
	}
}

