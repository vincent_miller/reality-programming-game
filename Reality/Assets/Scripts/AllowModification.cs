﻿using UnityEngine;
using System.Collections;

public class AllowModification : MonoBehaviour, AccessibilityProvider {

	public bool canMove, canImpel, canRotate, canFace, canScale, canKill;

	public bool CanMove () {
		return canMove;
	}

	public bool CanImpel () {
		return canImpel;
	}

	public bool CanRotate () {
		return canRotate;
	}

	public bool CanFace () {
		return canFace;
	}

	public bool CanScale () {
		return canScale;
	}
	
	public bool CanKill () {
		return canKill;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
