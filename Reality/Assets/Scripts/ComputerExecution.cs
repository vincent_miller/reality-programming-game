﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ComputerExecution : MonoBehaviour {

	public interface Executable {
		void execute(ComputerExecution ex);
	}

	public LinkedList<Executable> execQueue = new LinkedList<Executable> ();
	public Stack<object> dataStack = new Stack<object> ();
	public Dictionary<string, object> variables = new Dictionary<string, object>();
	public LinkedList<int> energyPackets = new LinkedList<int> ();
	public const int maxQueue = 100;
	public string program;
	private bool isCaretCake;

	public bool IsCaretCake() {
		return isCaretCake;
	}

	public bool isRunning() {
		return execQueue.Count > 0;
	}

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (execQueue.Count > 0) {
			if (execQueue.Count > maxQueue) {
				stop();
				Debug.LogError("Queue full! Emptied!");
			} else {
				Executable next = execQueue.First.Value;
				execQueue.RemoveFirst();
				next.execute(this);
			}
		}
	}

	public void start() {
		if (program.StartsWith("#!/bin/~ATH\n")) {
			isCaretCake = false;
			TildeAthParser p = new TildeAthParser (program.Substring("#!/bin/~ATH\n".Length));
			while (!p.accept(TildeAthTokenizer.Token.END_OF_FILE)) {
				Statements.Statement st = p.nextStatement (false);
				if (st == null) {
					stop ();
					return;
				}
				execQueue.AddLast(st);
			}
			execQueue.AddLast (new EndOfProgramExec ());
			Context.SetupTildeAthVars (variables);
		} else {
			isCaretCake = true;
			CaretCakeParser p = new CaretCakeParser(program.StartsWith("#!/bin/^CAKE\n") ? program.Substring("#!/bin/^CAKE\n".Length) : program);
			while (!p.accept(CaretCakeTokenizer.Token.END_OF_FILE)) {
				Statements.Statement st = p.nextStatement();
				if (st == null) {
					stop ();
					return;
				}
				execQueue.AddLast(st);
			}
			Context.SetupCaretCakeVars(variables);
		}
	}

	public class EndOfProgramExec : Executable {
		public void execute(ComputerExecution ex) {
			Debug.LogError ("The program did not end with a THIS.DIE()!");
		}
	}

	public void stop() {
		execQueue.Clear ();
		dataStack.Clear ();
		energyPackets.Clear ();
	}
}
