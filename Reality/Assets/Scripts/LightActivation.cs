﻿using UnityEngine;
using System.Collections;

public class LightActivation : Triggerable {

	public bool lighting = true;

	// Use this for initialization
	void Start () {
		setLighting (lighting);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void setLighting(bool dolight) {
		foreach (Light t in this.GetComponentsInChildren<Light>()) {
			t.enabled = dolight;
		}
	}

	public override void OnTrigger(string command) {
		if (command == "enable") {
			lighting = true;
			setLighting(lighting);
		} else if (command == "disable") {
			lighting = false;
			setLighting(lighting);
		}
	}
}
