﻿using UnityEngine;
using System.Collections;

public class Fan : MonoBehaviour {
	public float force = 5;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 a = transform.localEulerAngles;
		a.z += 360 * Time.deltaTime;
		transform.localEulerAngles = a;
	}

	void OnTriggerStay(Collider other) {
		Rigidbody rb = other.GetComponent<Rigidbody>();
		print ("impelling");
		float height = GetComponent<BoxCollider>().size.y * transform.localScale.x;
		float fDrop = Vector3.Distance(transform.position, other.transform.position) / height;
		if (rb != null) {
			rb.velocity += transform.forward * force * Time.deltaTime / fDrop;
		}

		BlownByFan b = other.GetComponent<BlownByFan>();
		if (b != null) {
			//b.velocity += transform.forward * force * Time.deltaTime / fDrop;
		}
	}
}
