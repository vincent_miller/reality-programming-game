using System;
using System.Collections.Generic;

public class Statements {
	public interface Statement : ComputerExecution.Executable {
		string display();
	}
	
	public class ImportStatement : Statement {
		private string typename, varname;
		
		public ImportStatement(string typename, string varname) {
			this.typename = typename;
			this.varname = varname;
		}
		
		public string display() {
			return "import[" + typename + "," + varname + "]";
		}

		public void execute(ComputerExecution ex) {
			ex.variables [varname] = Context.Import (ex, typename, varname);
		}
	}

	public class BindStatement : Statement {
		private string var;
		private Expressions.Expression value;

		public BindStatement(string var, Expressions.Expression value) {
			this.var = var;
			this.value = value;
		}

		public string display() {
			return "bind[" + var + "," + value.display() + "]";
		}

		public void execute(ComputerExecution ex) {
			ex.execQueue.AddFirst (new BindContinuationStatement (var));
			ex.execQueue.AddFirst (value);
		}

		public class BindContinuationStatement : ComputerExecution.Executable {
			private string var;

			public BindContinuationStatement(string var) {
				this.var = var;
			}

			public void execute(ComputerExecution ex) {
				ex.variables [var] = ex.dataStack.Pop ();
			}
		}
	}
	
	public class LoopStatement : Statement {
		private Expressions.Expression condition;
		private List<Statement> body;
		private Statement executeStmt;
		
		public LoopStatement(Expressions.Expression condition, List<Statement> body, Statement executeStmt) {
			this.condition = condition;
			this.body = body;
			this.executeStmt = executeStmt;
		}
		
		public string display() {
			string outp = "~ath[" + condition.display () + ",";
			foreach (Statement st in body) {
				outp += st.display() + ",";
			}
			this.execute (null);
			return outp + executeStmt.display ();
		}
		
		public void execute(ComputerExecution ex) {
			ex.execQueue.AddFirst (new LoopContinuationExec(true, this));
			ex.execQueue.AddFirst (condition);
		}

		private class LoopContinuationExec : ComputerExecution.Executable {
			private bool first;
			private LoopStatement outer;
			private int energy;
			public LoopContinuationExec(bool first, LoopStatement outer) {
				this.first = first;
				this.outer = outer;
			}
			public void execute(ComputerExecution ex) {
				object top = ex.dataStack.Pop ();
				if (first) {
					this.energy = Context.GetDeathEnergy(top);
				}
				if (Context.IsAlive (top)) {
					// Continue looping
					ex.execQueue.AddFirst(first ? new LoopContinuationExec(false, outer) : this);
					ex.execQueue.AddFirst(outer.condition);
					int i;
					for (i=outer.body.Count - 1; i>=0; i--) {
						ex.execQueue.AddFirst(outer.body[i]);
					}
				} else {
					if (!first) { // Only run EXECUTE() if it was alive at some point during the loop
						ex.energyPackets.AddLast(energy);
						ex.execQueue.AddFirst (new DropEnergyPacketExec());
						ex.execQueue.AddFirst(outer.executeStmt);
					}
				}
			}
		}

		private class DropEnergyPacketExec : ComputerExecution.Executable {
			public void execute(ComputerExecution ex) {
				ex.energyPackets.RemoveLast ();
			}
		}
	}
	
	public class ExpressionStatement : Statement {
		private Expressions.Expression value;
		
		public ExpressionStatement(Expressions.Expression value) {
			this.value = value;
		}
		
		public string display() {
			return "expression[" + value.display () + "]";
		}

		public void execute(ComputerExecution ex) {
			value.execute (ex);
		}
	}
}
