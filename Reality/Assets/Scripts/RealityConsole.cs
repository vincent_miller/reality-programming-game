﻿using UnityEngine;
using System.Collections;

public class RealityConsole : MonoBehaviour {
	private GameObject computer = null;
	public Texture2D open;
	public Texture2D closed;
	public Font mono;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.E)) {
			if (computer != null) {
				setLocked(false);
				computer = null;
			} else {
				RaycastHit info;
				bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2)), out info, 2);
				if (hit && info.transform.gameObject.GetComponent<ComputerExecution>() != null) {
					computer = info.transform.gameObject;

					setLocked (true);
				}
			}
		}
	}

	void setLocked(bool disabled) {
		CharacterMotor motor = gameObject.GetComponent<CharacterMotor>();
		MouseLook mouse1 = gameObject.GetComponent<MouseLook>();
		MouseLook mouse2 = transform.FindChild("Main Camera").GetComponent<MouseLook>();
		
		motor.enabled = !disabled;
		mouse1.enabled = !disabled;
		mouse2.enabled = !disabled;
	}

	void OnGUI() {
		if (computer != null) {
			GUIStyle fontStyle = new GUIStyle(GUI.skin.textArea);
			fontStyle.font = mono;//new Font ("Monospaced");
			//GUI.skin.font = mono;
			ComputerExecution ex = computer.GetComponent<ComputerExecution>();
			string outp = GUI.TextArea(new Rect(50, 25, Screen.width - 100, Screen.height - 100), ex.program, fontStyle);
			if (!ex.isRunning()) {
				ex.program = outp;
			}
			bool pressed = GUI.Button(new Rect(50, Screen.height - 75, 100, 25), ex.isRunning() ? "Terminate" : "Execute");
			if (pressed) {
				if (!ex.isRunning()) {
					ex.start();
				} else {
					ex.stop();
				}
			}

			bool pressed2 = GUI.Button(new Rect(175, Screen.height - 75, 100, 25), "Close");
			if (pressed2) {
				computer = null;
				setLocked(false);
			}
		} else {
			if (selectingInteractable()) {
				GUI.color = Color.blue;
			} else {
				GUI.color = Color.red;
			}
			bool held = gameObject.GetComponent<PickupObjects>().held != null;
			GUI.DrawTexture(new Rect(Screen.width / 2 - open.width / 2, Screen.height / 2 - open.height / 2, open.width, open.height), held ? closed : open);
			
		}
	}

	bool selectingInteractable() {
		RaycastHit hit;
		if (Physics.Raycast(Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2)), out hit, 2)) {
			GameObject obj = hit.transform.gameObject;
			return (obj.GetComponent<Rigidbody>() != null || obj.GetComponent<PedestalButton>() != null || obj.GetComponent<ComputerExecution>() != null);
			
			
		} else {
			return false;
		}
	}
}
