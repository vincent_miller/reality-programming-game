﻿using UnityEngine;
using System.Collections;

public class CubeSpawner : Triggerable {
	public GameObject prefab;
	public bool CanMove;
	public bool CanRotate;
	public bool CanScale;
	public bool CanImpel;
	public bool CanFace;
	public bool CanKill;

	private GameObject cube;

	public bool autoSpawn;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if ((cube == null || cube.gameObject == null) && autoSpawn) {
			spawn ();
		}
	}

	public override void OnTrigger(string command) {
		if (command == "spawn") {
			if (cube != null) {
				Destroy (cube);
			}

			spawn();
		}
	}

	private void spawn() {
		cube = (GameObject)Instantiate(prefab);
		cube.transform.position = transform.position;
		cube.transform.localEulerAngles = Vector3.zero;
		cube.name = "cube";
		cube.AddComponent<AllowModification>();
		AllowModification am = cube.GetComponent<AllowModification>();
		am.canMove = CanMove;
		am.canRotate = CanRotate;
		am.canScale = CanScale;
		am.canImpel = CanImpel;
		am.canFace = CanFace;
		am.canKill = CanKill;
	}
}
