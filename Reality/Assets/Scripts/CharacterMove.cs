﻿using UnityEngine;
using System.Collections;

public class CharacterMove : MonoBehaviour {
	public float speedForward = 4;
	public float speedLeft = 4;
	public float jumpSpeed = 2;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			if (IsGrounded()) {
				transform.rigidbody.velocity -= Physics.gravity.normalized * jumpSpeed;
			}
		}
	}

	void FixedUpdate() {
		gameObject.rigidbody.position += transform.forward * Time.deltaTime * Input.GetAxis("Vertical") * speedForward;
		gameObject.rigidbody.position += transform.right * Time.deltaTime * Input.GetAxis("Horizontal") * speedLeft;
	}

	bool IsGrounded() {
		return Physics.Raycast (new Ray(transform.position, -transform.up), 1.1f);
	}
}
