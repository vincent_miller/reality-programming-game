﻿using UnityEngine;
using System.Collections;

public class TriggerZone : MonoBehaviour {
	public GameObject Notify;
	public string onEnter;
	public string inZone;
	public string onExit;

	public void OnTriggerEnter(Collider other) {
		if (Notify != null) {
			Notify.GetComponent<Triggerable>().OnTrigger(onEnter);
			}
	}

	public void OnTriggerStay (Collider other) {
		if (Notify != null) {
			Notify.GetComponent<Triggerable>().OnTrigger(inZone);
			}
	}

	public void OnTriggerExit (Collider other) {
		if (Notify != null) {
			Notify.GetComponent<Triggerable>().OnTrigger(onExit);
		}
	}
}
