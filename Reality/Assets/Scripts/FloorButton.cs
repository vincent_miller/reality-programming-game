﻿using UnityEngine;
using System.Collections;

public class FloorButton : MonoBehaviour {
	public GameObject Notify;
	public GameObject Up;
	public GameObject Down;
	public string OnDown;
	public string OnUp;

	private int numPresses = 0;
	
	void OnTriggerEnter() {
		numPresses++;
		if (numPresses == 1) {
			if (Notify != null) {
				Notify.GetComponent<Triggerable>().OnTrigger(OnDown);
			}
			Destroy (transform.FindChild("model").gameObject);
			GameObject newModel = (GameObject)Instantiate(Down);
			newModel.transform.parent = transform;
			newModel.name = "model";
			newModel.transform.position = transform.position;
			newModel.transform.eulerAngles = transform.eulerAngles;
			newModel.transform.localScale = new Vector3(1, 1, 1);
		}
	}

	void OnTriggerExit() {
		numPresses --;
		if (numPresses == 0) {
			if (Notify != null) {
				Notify.GetComponent<Triggerable>().OnTrigger(OnUp);
			}
			Destroy (transform.FindChild("model").gameObject);
			GameObject newModel = (GameObject)Instantiate(Up);
			newModel.transform.parent = transform;
			newModel.name = "model";
			newModel.transform.position = transform.position;
			newModel.transform.eulerAngles = transform.eulerAngles;
			newModel.transform.localScale = new Vector3(1, 1, 1);
		}
	}
}
