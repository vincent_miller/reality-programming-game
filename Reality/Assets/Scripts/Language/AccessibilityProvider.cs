public interface AccessibilityProvider {
	bool CanMove();
	bool CanImpel();
	bool CanRotate();
	bool CanFace();
	bool CanScale();
	bool CanKill();
}
