﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {
	public string level1;
	public Texture2D logo;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 a = transform.localEulerAngles;
		a.y += 10 * Time.deltaTime;
		transform.localEulerAngles = a;
	}

	void OnGUI() {
		GUI.DrawTexture(new Rect(Screen.width - 300, 50, logo.width, logo.height), logo);
		if (GUI.Button(new Rect(Screen.width - 200, 200, 150, 40), "Start")) {
			Screen.showCursor = false;
			Application.LoadLevel (level1);
		}

		if (GUI.Button(new Rect(Screen.width - 200, 250, 150, 40), "Quit")) {
			Application.Quit();
		}
	}
}
