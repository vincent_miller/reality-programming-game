﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Platform : Triggerable {

	public float amplitude = 5;
	public float speed = .2f;
	public Vector3 direction = Vector3.forward;
	private Vector3 p0;
	private float rel = 0;
	private Vector3 last;
	public bool platform_enabled;
	private List<Collider> others = new List<Collider>();

	// Use this for initialization
	void Start () {
		p0 = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (platform_enabled) {
			Vector3 dir = transform.TransformDirection(direction);
			double nrel = (speed * (Time.time + rel) + 1) % 4;
			float vel = 0;
			if (nrel >= 2) {
				nrel = 3 - nrel;
				vel = -speed * amplitude;
			} else {
				nrel = nrel - 1;
				vel = speed * amplitude;
			}
			//double orel = Math.Sin (Math.PI * 2 * speed * (Time.time - Time.deltaTime + rel));
			transform.position = p0 + amplitude * dir * (float) nrel;
			//float mul = (float) (amplitude * (nrel - orel));
			//float alpha = mul * dir.x;
			//Vector3 relM = new Vector3(alpha, mul * dir.y, mul * dir.z);
			Vector3 relM = transform.position - last;
			//print ("Move " + dir + " -> " + relM + "/" + alpha + "/" + relM.x + " now " + nrel + " was " + orel + " means " + mul);
			foreach (Collider coll in others) {
				if (coll.rigidbody != null) {
					coll.rigidbody.velocity = new Vector3(0, 0, 0);
				}
				coll.transform.position += relM;
			}
			last = transform.position;
		}
	}

	void OnTriggerEnter(Collider other) {
		others.Add (other);
	}

	void OnTriggerExit(Collider other) {
		others.Remove(other);
	}

	public override void OnTrigger(string command) {
		if (command == "enable" && !platform_enabled) {
			platform_enabled = true;
			last = transform.position;
			rel -= Time.time;
		}
		
		if (command == "disable" && platform_enabled) {
			platform_enabled = false;
			rel += Time.time;
		}
	}
}
