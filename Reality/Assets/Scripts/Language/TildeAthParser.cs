using System;
using System.Collections.Generic;
using UnityEngine;

public class TildeAthParser {

	private TildeAthTokenizer tkn;

	public TildeAthParser (string data) {
		this.tkn = new TildeAthTokenizer (data);
	}

	public bool accept(TildeAthTokenizer.Token t) {
		TildeAthTokenizer.Token found = tkn.nextToken();
		if (found == t) {
			return true;
		} else {
			tkn.push(found);
			return false;
		}
	}

	public Statements.Statement nextStatement(bool canDropSemicolon) {
		if (accept (TildeAthTokenizer.Token.IMPORT)) {
			if (!accept (TildeAthTokenizer.Token.SYMBOL)) {
				Debug.LogError("Failed 148");
				return null;
			}
			string tname = (string) tkn.getAssociated ();
			if (!accept (TildeAthTokenizer.Token.SYMBOL)) {
				Debug.LogError("Failed 153");
				return null;
			}
			string fname = (string) tkn.getAssociated ();
			if (!accept (TildeAthTokenizer.Token.SEMI) && !canDropSemicolon) {
				Debug.LogError("Failed 158");
				return null;
			}
			return new Statements.ImportStatement (tname, fname);
		} else if (accept (TildeAthTokenizer.Token.TILDEATH)) {
			if (!accept (TildeAthTokenizer.Token.LP)) {
				Debug.LogError("Failed 164");
				return null;
			}
			Expressions.Expression condition = nextExpression();
			if (condition == null) {
				Debug.LogError("Failed 169");
				return null;
			}
			if (!accept (TildeAthTokenizer.Token.RP)) {
				Debug.LogError("Failed 173");
				return null;
			}
			if (!accept (TildeAthTokenizer.Token.LCB)) {
				Debug.LogError("Failed 177");
				return null;
			}
			List<Statements.Statement> body = new List<Statements.Statement>();
			while (!accept (TildeAthTokenizer.Token.RCB)) {
				Statements.Statement stmt = nextStatement(false);
				if (stmt == null) {
					Debug.LogError("Failed 184");
            		return null;
	            }
				body.Add(stmt);
			}
			if (!accept (TildeAthTokenizer.Token.EXECUTE)) {
				Debug.LogError("Failed 190");
				return null;
			}
			if (!accept (TildeAthTokenizer.Token.LP)) {
				Debug.LogError("Failed 194");
				return null;
			}
			Statements.Statement execute = nextStatement(true);
			if (execute == null) {
				Debug.LogError("Failed 199");
				return null;
			}
			if (!accept (TildeAthTokenizer.Token.RP)) {
				Debug.LogError("Failed 203");
				return null;
			}
			if (!accept(TildeAthTokenizer.Token.SEMI) && !canDropSemicolon) {
				Debug.LogError("Failed 207");
				return null;
			}
			return new Statements.LoopStatement(condition, body, execute);
		} else if (accept (TildeAthTokenizer.Token.BIND)) {
			if (!accept (TildeAthTokenizer.Token.SYMBOL)) {
				Debug.LogError("Failed 91");
				return null;
			}
			string var = (string) tkn.getAssociated();
			if (!accept (TildeAthTokenizer.Token.SET)) {
				Debug.LogError("Failed 96");
				return null;
			}
			Expressions.Expression ex = nextExpression();
			if (ex == null) {
				Debug.LogError("Failed 101");
				return null;
			}
			if (!accept(TildeAthTokenizer.Token.SEMI) && !canDropSemicolon) {
				Debug.LogError("Failed 218: " + ex.display());
				return null;
			}
			return new Statements.BindStatement(var, ex);
		//} else if (accept(TildeAthTokenizer.Token.SET)) {
			//return new Statements.
		} else {
			Expressions.Expression ex = nextExpression();
			if (ex == null) {
				Debug.LogError("Failed 214");
				return null;
			}
			if (!accept(TildeAthTokenizer.Token.SEMI) && !canDropSemicolon) {
				Debug.LogError("Failed 218: " + ex.display());
				return null;
			}
			return new Statements.ExpressionStatement(ex);
		}
	}

	public Expressions.Expression nextExpression() {
		Expressions.Expression term = nextTerm();
		if (term == null) {
			Debug.LogError("Failed 228");
			return null;
		}
		while (true) {
			if (accept (TildeAthTokenizer.Token.DOT)) {
				if (!accept (TildeAthTokenizer.Token.SYMBOL)) {
					Debug.LogError("Failed 234");
					return null;
				}
				term = new Expressions.FieldFetchExpression(term, (string) tkn.getAssociated());
			} else if (accept (TildeAthTokenizer.Token.LP)) {
				List<Expressions.Expression> arguments = new List<Expressions.Expression>();
				if (!accept(TildeAthTokenizer.Token.RP)) {
					// Argument list
					do {
						Expressions.Expression e = nextExpression();
						if (e == null) {
							Debug.LogError("Failed 245");
							return null;
						}
						arguments.Add(e);
					} while (accept(TildeAthTokenizer.Token.COMMA));
					if (!accept(TildeAthTokenizer.Token.RP)) {
						Debug.LogError("Failed 251");
						return null;
					}
				}
				term = new Expressions.InvokeExpression(term, arguments);
			} else {
				return term;
			}
		}
	}

	public Expressions.Expression nextTerm() {
		if (accept (TildeAthTokenizer.Token.NUMBER)) {
			return new Expressions.NumberConstantExpression ((float) tkn.getAssociated ());
		} else if (accept (TildeAthTokenizer.Token.SYMBOL)) {
			return new Expressions.VariableFetchExpression ((string) tkn.getAssociated ());
		} else if (accept (TildeAthTokenizer.Token.LP)) {
			Expressions.Expression outp = nextExpression ();
			if (outp == null || !accept (TildeAthTokenizer.Token.RP)) {
				Debug.LogError("Failed 270");
				return null;
			}
			return outp;
		} else if (accept(TildeAthTokenizer.Token.THIS)) {
			return new Expressions.ThisExpression();
		} else if (accept(TildeAthTokenizer.Token.NULL)) {
			return new Expressions.NullExpression();
		} else {
			TildeAthTokenizer.Token peek = tkn.nextToken();
			tkn.push(peek);
			Debug.LogError("Failed 279 with " + peek);
			return null;
		}
	}
}
