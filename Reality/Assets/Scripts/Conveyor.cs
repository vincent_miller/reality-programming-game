﻿using UnityEngine;
using System.Collections;

public class Conveyor : Triggerable {
	public float speed = 5;
	public bool startEnabled = true;

	// Use this for initialization
	void Start () {
		enabled = startEnabled;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerStay(Collider other) {
		if (enabled) {
			other.transform.position += transform.right * speed * Time.deltaTime;
		}
	}

	public override void OnTrigger(string command) {
		if (command == "start") {
			enabled = true;
		} else if (command == "stop") {
			enabled = false;
		}
	}
}
