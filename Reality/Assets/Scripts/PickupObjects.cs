﻿using UnityEngine;
using System.Collections;

public class PickupObjects : MonoBehaviour {

	public GameObject held { get; private set; }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.E)) {
			RaycastHit info;
			bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2)), out info, 2);

			if (hit && held == null) {
				Rigidbody rb = info.transform.GetComponent<Rigidbody>();
				if (rb != null) {
					pickup(rb.gameObject);
				}
			} else {
				if (held != null) {
					drop();
				}
			}
		}
		
		if (held != null && Vector3.Distance(transform.position, held.transform.position) > 3) {
			drop();
		}
	}

	void FixedUpdate() {
		if (held != null) {
			held.rigidbody.velocity = Vector3.zero;
			held.rigidbody.angularVelocity = Vector3.zero;
			Transform t = transform.FindChild("Main Camera");
			held.rigidbody.position = Vector3.MoveTowards(held.rigidbody.position, t.position + t.forward * 1.5f, Time.deltaTime * 10);
			held.transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, 0);
		}
	}

	private void pickup(GameObject obj) {
		if (held != null) {
			drop();
		}

		held = obj;
		held.rigidbody.useGravity = false;
		//obj.transform.parent = transform.transform;
	}

	private void drop() {
		held.rigidbody.useGravity = true;
		//held.transform.parent = null;
		held = null;
	}
}
