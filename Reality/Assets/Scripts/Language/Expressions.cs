using System;
using System.Collections.Generic;
using UnityEngine;

public class Expressions {
	
	public interface Expression : ComputerExecution.Executable {
		string display();
	}
	
	public class FieldFetchExpression : Expression {
		private Expression expr;
		private string attribute;
		
		public FieldFetchExpression(Expression expr, string attribute) {
			this.expr = expr;
			this.attribute = attribute;
		}
		
		public string display() {
			return "fetch[" + expr.display() + "," + attribute + "]";
		}

		public void execute(ComputerExecution ex) {
			ex.execQueue.AddFirst (new FieldFetchContinuationExec (attribute));
			ex.execQueue.AddFirst (expr);
		}

		public class FieldFetchContinuationExec : ComputerExecution.Executable {
			private string attribute;

			public FieldFetchContinuationExec(string attribute) {
				this.attribute = attribute;
			}

			public void execute(ComputerExecution ex) {
				if (ex.IsCaretCake()) {
					ex.dataStack.Push (Context.FieldFetchCaretCake (ex, ex.dataStack.Pop (), attribute));
				} else {
					ex.dataStack.Push (Context.FieldFetchTildeAth (ex, ex.dataStack.Pop (), attribute));
				}
			}
		}
	}
	
	public class InvokeExpression : Expression {
		private Expression target;
		private List<Expression> arguments;
		
		public InvokeExpression(Expression target, List<Expression> arguments) {
			this.target = target;
			this.arguments = arguments;
		}
		
		public string display() {
			string outp = "invoke[" + target.display();
			foreach (Expression e in arguments) {
				outp += "," + e.display();
			}
			return outp + "]";
		}
		
		public void execute(ComputerExecution ex) {
			ex.execQueue.AddFirst (new InvokeContinuationExpression (arguments.Count));
			int i;
			for (i=arguments.Count - 1; i>=0; i--) {
				ex.execQueue.AddFirst(arguments[i]);
			}
			ex.execQueue.AddFirst (target);
		}

		public class InvokeContinuationExpression : ComputerExecution.Executable {
			private int count;
			public InvokeContinuationExpression(int count) {
				this.count = count;
			}
			public void execute(ComputerExecution ex) {
				object[] args = new object[count];
				for (int i=args.Length - 1; i>=0; i--) {
					args[i] = ex.dataStack.Pop();
				}
				ex.dataStack.Push (Context.Invoke (ex.dataStack.Pop (), args));
			}
		}
	}
	
	public class NumberConstantExpression : Expression {
		private float value;
		
		public NumberConstantExpression(float value) {
			this.value = value;
		}
		
		public string display() {
			return "integer[" + value + "]";
		}

		public void execute(ComputerExecution ex) {
			ex.dataStack.Push (value);
		}
	}
	
	public class VariableFetchExpression : Expression {
		public string var;
		
		public VariableFetchExpression(string var) {
			this.var = var;
		}
		
		public string display() {
			return "get[" + var + "]";
		}
		
		public void execute(ComputerExecution ex) {
			if (ex.variables.ContainsKey (var)) {
				ex.dataStack.Push (ex.variables [var]);
			} else {
				Debug.Log("No such variable: " + var);
				ex.dataStack.Push (null);
			}
		}
	}
	
	public class ThisExpression : Expression {
		
		public string display() {
			return "this[]";
		}
		
		public void execute(ComputerExecution ex) {
			ex.dataStack.Push (Context.GetThis(ex));
		}
	}
	
	public class NullExpression : Expression {
		
		public string display() {
			return "null[]";
		}
		
		public void execute(ComputerExecution ex) {
			ex.dataStack.Push (null);
		}
	}
}
